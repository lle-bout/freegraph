/*
 *   ___   __ __  _____  ____   ______  __ __ 
 *  /   \ |  |  ||     ||    \ |      ||  |  |
 * |     ||  |  ||__/  ||  o  )|      ||  |  |
 * |  O  ||  |  ||   __||     ||_|  |_||  ~  |
 * |     ||  :  ||  /  ||  O  |  |  |  |___, |
 * |     ||     ||     ||     |  |  |  |     |
 *  \___/  \__,_||_____||_____|  |__|  |____/ 
 * 
 *            ouzb65ty@protonmail.ch
 */

#ifndef FREEGRAPH_H
# define FREEGRAPH_H
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <errno.h>
# include <editline.h>
# include <ctype.h>
# include <unistd.h>
# include "list.h"

/* Lexer */

enum identifier_e {
	NUMBER, STRING, LABEL, BUILTIN
};
typedef enum identifier_e identifier_t;

union value_u {
	int number;
	char *string;
	char *label;
	char *builtin;
};
typedef union value_u value_t;

struct lex_s {
	value_t value;
	identifier_t ident;
	char *token;
	struct lex_s *next;
};
typedef struct lex_s lex_t;

void
print_lex(lex_t *lex);
void
free_lex(lex_t *lex);
lex_t *
lexer(const char *line);

/* Parser */

struct synt_s {
	struct lex_s *lex;
	struct synt_s *left;
	struct synt_s *right;
};
typedef struct synt_s synt_t;

synt_t *
lparser(lex_t *lex);

#endif
