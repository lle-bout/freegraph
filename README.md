# freegraph

Free Oriented Graph Database

## Example

	# New graph
	CREATE story GRAPH

	# Work with it
	USE story

	# New vertex in story
	CREATE unknown VERTEX (
		INT age 100,
		STRING name "unknown"
	)
	CREATE person VERTEX
	CREATE bob VERTEX (
		INT age 22,
		STRING name "Bob"
	)

	CREATE lea VERTEX (
		INT age 21,
		STRING name "Lea"
	)

	# New edge in story
	CREATE is EDGE
	CREATE friend EDGE (
		INT since 1561919318
	)

	# Duplicate fastly
	DUPLICATE unknown(19, "Jean") AS jean
	DUPLICATE unknown(_, "Lola") AS lola

	# Connect vertex with edge
	CONNECT bob is person
	CONNECT lea is person
	CONNECT lola is person
	CONNECT bob friend lea
	CONNECT lea friend bob
	CONNECT lola friend bob

	# Debug
	SHOW story GRAPH
	SHOW lea VERTEX
	SHOW friend EDGE

	# Remove
	REMOVE person
