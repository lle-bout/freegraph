/*
 *   ___   __ __  _____  ____   ______  __ __ 
 *  /   \ |  |  ||     ||    \ |      ||  |  |
 * |     ||  |  ||__/  ||  o  )|      ||  |  |
 * |  O  ||  |  ||   __||     ||_|  |_||  ~  |
 * |     ||  :  ||  /  ||  O  |  |  |  |___, |
 * |     ||     ||     ||     |  |  |  |     |
 *  \___/  \__,_||_____||_____|  |__|  |____/ 
 * 
 *            ouzb65ty@protonmail.ch
 */

#include "inc/freegraph.h"

static int
isnumber(char *s) {

	int len = strlen(s);

	for (int i = 0; i < len; i++) {
		if (!isdigit(s[i])) {
			return (0);
		}
	}
	return (1);
}

static int
isbuiltin(char *s) {
	if (strcmp(s, "CREATE") == 0)
		return (1);
	else
		return (0);
}

void
print_lex(lex_t *lex) {

	lex_t *item = NULL;

	SL_FOREACH(lex, item){
		fflush(0);
		if (item->ident == 0)
			printf("%d <number>\n", item->value.number);
		else if (item->ident == 1)
			printf("%s <string>\n", item->value.string);
		else if (item->ident == 2)
			printf("%s <label>\n", item->value.label);
		else if (item->ident == 3)
			printf("%s <BUILTIN>\n", item->value.builtin);
	}
	return ;
}

void
free_lex(lex_t *lex) {

	lex_t *item = NULL;
	lex_t *tmp = NULL;

	SL_FOREACH(lex, item){
		free(item->token);
		free(item);
		SL_DELETE(lex, item);
	}
}

lex_t *
lexer(const char *line) {

	size_t len = strlen(line);
	char *token = NULL;
	size_t n = 0;
	lex_t *lex = NULL;
	lex_t *item = NULL;

	for (size_t i = 0; i < len; i++) {
		if (isalnum(line[i])) {
			/* Get label */
			for (n = 0; isalnum(line[i + n]) && line[i]; n++) {}
			token = strndup(&line[i], n);
			item = (lex_t *)malloc(sizeof(lex_t));
			if (item == NULL)
				return (NULL);
			if (isnumber(token)) {
				item->value.number = atoi(token);
				item->ident = 0;
			} else if (isbuiltin(token)) {
				item->value.builtin = token;
				item->ident = 3;
			} else {
				item->value.label = token;
				item->ident = 2;
			}
		} else if (line[i] == '"') {
			++i;
			/* Get double quotes strings */
			for (n = 0; isascii(line[i + n]) && line[i]; n++) {
				if (line[i + n] == '"')
					break ;
			}
			token = strndup(&line[i], n);
			item = (lex_t *)malloc(sizeof(lex_t));
			if (item == NULL)
				return (NULL);
			item->value.string = token;
			item->ident = 1;
		} else if (line[i] == '#') {
			/* Skip commentary */
			for (; isascii(line[i]) && line[i]; i++) {
				if (line[i] == '\n')
					break ;
			}
		}
		if (token) {
			item->next = NULL;
			item->token = token;
			SL_APPEND(lex, item);
			token = NULL;
		}
		i += n;
		n = 0;
	}
	return lex;
}
